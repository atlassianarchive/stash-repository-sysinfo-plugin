package com.atlassian.stash.plugin.reposysinfo;

import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.repository.Version;
import com.atlassian.stash.scm.git.GitScm;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.PermissionValidationService;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class RepositorySysInfoServlet extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(RepositorySysInfoServlet.class);
    private static final Version DEPRECATED_VERSION = new Version(2, 7);

    private final RepositoryService repositoryService;
    private final PermissionValidationService permissionValidationService;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final ApplicationPropertiesService applicationPropertiesService;
    private final boolean deprecated;

    public RepositorySysInfoServlet(RepositoryService repositoryService,
                                    PermissionValidationService permissionValidationService,
                                    SoyTemplateRenderer soyTemplateRenderer,
                                    ApplicationPropertiesService applicationPropertiesService) {
        this.repositoryService = repositoryService;
        this.permissionValidationService = permissionValidationService;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.applicationPropertiesService = applicationPropertiesService;

        boolean deprecated = false;
        String versionAsString = applicationPropertiesService.getBuildVersion();
        try {
            deprecated = new Version(versionAsString).compareTo(DEPRECATED_VERSION) >= 0;
        } catch (Exception e) {
            log.warn(String.format("Could not parse build version string %s", versionAsString), e);
        }
        this.deprecated = deprecated;

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        permissionValidationService.validateForGlobal(Permission.SYS_ADMIN);
        Repository repository = getRepository(req);
        if (repository == null) {
            res.sendError(404);
            return;
        }

        String repositorySize = getRepositorySize(repository);
        File repositoryDirectory = getRepositoryDirectory(repository);
        List<String> repositoryHooks = findRepositoryHooks(repository, repositoryDirectory);
        String repositoryLocation = repositoryDirectory.getAbsolutePath();
        res.setContentType("text/html;charset=UTF-8");
        try {
            soyTemplateRenderer.render(
                    res.getWriter(),
                    "com.atlassian.stash.plugin.repository-sysinfo-plugin:server-side-soy",
                    "plugin.page.repoSysInfo",
                    ImmutableMap.<String, Object>builder()
                            .put("repository", repository)
                            .put("repositorySize", repositorySize)
                            .put("repositoryLocation", repositoryLocation)
                            .put("repositoryHooks", repositoryHooks)
                            .put("deprecated", deprecated)
                            .build()
            );
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            } else {
                throw new ServletException(e);
            }
        }
    }

    private Repository getRepository(HttpServletRequest req) {
        String pathInfo = req.getPathInfo();
        if (pathInfo == null) {
            return null;
        }
        pathInfo = pathInfo.startsWith("/") ? pathInfo.substring(0) : pathInfo;
        String[] pathParts = pathInfo.split("/");
        if (pathParts.length != 3) {
            return null;
        }
        return repositoryService.findBySlug(pathParts[1], pathParts[2]);
    }

    protected String getRepositorySize(Repository repository) {
        double size = repositoryService.getSize(repository);
        String unit = "B";
        if (size > 1024) {
            size /= 1024;
            unit = "kB";
            if (size > 1024) {
                size /= 1024;
                unit = "MB";
                if (size > 1024) {
                    size /= 1024;
                    unit = "GB";
                }
            }
        }
        return String.format("%.2f %s", size, unit);
    }

    protected File getRepositoryDirectory(Repository repository) {
        return applicationPropertiesService.getRepositoryDir(repository);
    }

    protected List<String> findRepositoryHooks(Repository repository, File repositoryDirectory) {
        if (GitScm.ID.equals(repository.getScmId())) {
            File hooksDirectory = new File(repositoryDirectory, "hooks");
            if (hooksDirectory.isDirectory()) {
                return Arrays.asList(hooksDirectory.list(new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        File file = new File(dir, name);
                        return file.isFile() && file.canExecute() && !name.endsWith(".sample");
                    }
                }));
            }
        }
        return Collections.emptyList();
    }

}
